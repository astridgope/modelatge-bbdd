-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema Opticians
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `Opticians` ;

-- -----------------------------------------------------
-- Schema Opticians
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `Opticians` DEFAULT CHARACTER SET utf8 COLLATE utf8_spanish_ci ;
USE `Opticians` ;

-- -----------------------------------------------------
-- Table `Opticians`.`Supplier`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Supplier` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Supplier` (
  `idSupplier` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `addressStreet` VARCHAR(45) NULL,
  `addressNumber` INT NULL,
  `addressFloor` VARCHAR(45) NULL,
  `addressDoor` VARCHAR(45) NULL,
  `addressCity` VARCHAR(45) NULL,
  `addressZipcode` INT NULL,
  `addressCountry` VARCHAR(45) NULL,
  `phone` INT NULL,
  `fax` INT NULL,
  `nif` VARCHAR(45) NULL,
  PRIMARY KEY (`idSupplier`),
  UNIQUE INDEX `idSupplier_UNIQUE` (`idSupplier` ASC) VISIBLE,
  UNIQUE INDEX `nif_UNIQUE` (`nif` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Brand`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Brand` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Brand` (
  `idBrand` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `Supplier_idSupplier` INT NOT NULL,
  PRIMARY KEY (`idBrand`),
  INDEX `fk_Brand_Supplier1_idx` (`Supplier_idSupplier` ASC) VISIBLE,
  UNIQUE INDEX `idbrand_UNIQUE` (`idBrand` ASC) VISIBLE,
  CONSTRAINT `fk_Brand_Supplier1`
    FOREIGN KEY (`Supplier_idSupplier`)
    REFERENCES `Opticians`.`Supplier` (`idSupplier`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`FrameModel`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`FrameModel` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`FrameModel` (
  `idFrameModel` INT NOT NULL,
  `Brand_idBrand` INT NOT NULL,
  `frame` VARCHAR(45) NULL,
  `frameColor` VARCHAR(45) NULL,
  `framePrice` FLOAT NOT NULL,
  PRIMARY KEY (`idFrameModel`),
  INDEX `fk_FrameModel_Brand1_idx` (`Brand_idBrand` ASC) INVISIBLE,
  UNIQUE INDEX `idFrameModel_UNIQUE` (`idFrameModel` ASC) VISIBLE,
  CONSTRAINT `fk_FrameModel_Brand1`
    FOREIGN KEY (`Brand_idBrand`)
    REFERENCES `Opticians`.`Brand` (`idBrand`)
    ON DELETE RESTRICT
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Glasses`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Glasses` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Glasses` (
  `idGlasses` INT NOT NULL,
  `gradLeftGlass` FLOAT NULL,
  `gradRightGlass` FLOAT NULL,
  `glassesColor` VARCHAR(45) NULL,
  `glassesPrice` FLOAT NULL,
  `FrameModel_idFrameModel` INT NOT NULL,
  PRIMARY KEY (`idGlasses`),
  INDEX `fk_Glasses_FrameModel1_idx` (`FrameModel_idFrameModel` ASC) VISIBLE,
  UNIQUE INDEX `idGlasses_UNIQUE` (`idGlasses` ASC) VISIBLE,
  CONSTRAINT `fk_Glasses_FrameModel1`
    FOREIGN KEY (`FrameModel_idFrameModel`)
    REFERENCES `Opticians`.`FrameModel` (`idFrameModel`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Employee`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Employee` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Employee` (
  `idEmployee` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  PRIMARY KEY (`idEmployee`),
  UNIQUE INDEX `idEmployee_UNIQUE` (`idEmployee` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Customer` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Customer` (
  `idCustomer` INT NOT NULL,
  `name` VARCHAR(45) NULL,
  `address` VARCHAR(100) NULL,
  `phone` VARCHAR(45) NULL,
  `email` VARCHAR(45) NULL,
  `signUpDate` DATE NULL,
  PRIMARY KEY (`idCustomer`),
  UNIQUE INDEX `idCustomer_UNIQUE` (`idCustomer` ASC) VISIBLE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Recommendation`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Recommendation` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Recommendation` (
  `idRecommendation` INT NOT NULL,
  `recommendedCustomer` INT NOT NULL,
  `recommendedBy` INT NOT NULL,
  INDEX `fk_Recommendation_Customer1_idx` (`recommendedCustomer` ASC) VISIBLE,
  INDEX `fk_Recommendation_Customer2_idx` (`recommendedBy` ASC) VISIBLE,
  PRIMARY KEY (`idRecommendation`),
  UNIQUE INDEX `idRecommendation_UNIQUE` (`idRecommendation` ASC) VISIBLE,
  UNIQUE INDEX `recommendedCustomer_UNIQUE` (`recommendedCustomer` ASC) VISIBLE,
  CONSTRAINT `fk_Recommendation_Customer1`
    FOREIGN KEY (`recommendedCustomer`)
    REFERENCES `Opticians`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_Recommendation_Customer2`
    FOREIGN KEY (`recommendedBy`)
    REFERENCES `Opticians`.`Customer` (`idCustomer`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Opticians`.`Invoice`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `Opticians`.`Invoice` ;

CREATE TABLE IF NOT EXISTS `Opticians`.`Invoice` (
  `idInvoice` INT NOT NULL,
  `Glasses_idGlasses` INT NOT NULL,
  `Customer_idCustomer` INT NOT NULL,
  `Employee_idEmployee` INT NOT NULL,
  PRIMARY KEY (`idInvoice`),
  INDEX `fk_Invoice_Employee1_idx` (`Employee_idEmployee` ASC) VISIBLE,
  INDEX `fk_Invoice_Customer1_idx` (`Customer_idCustomer` ASC) VISIBLE,
  INDEX `fk_Invoice_Glasses1_idx` (`Glasses_idGlasses` ASC) VISIBLE,
  UNIQUE INDEX `idInvoice_UNIQUE` (`idInvoice` ASC) VISIBLE,
  CONSTRAINT `fk_Invoice_Employee1`
    FOREIGN KEY (`Employee_idEmployee`)
    REFERENCES `Opticians`.`Employee` (`idEmployee`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Invoice_Customer1`
    FOREIGN KEY (`Customer_idCustomer`)
    REFERENCES `Opticians`.`Customer` (`idCustomer`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Invoice_Glasses1`
    FOREIGN KEY (`Glasses_idGlasses`)
    REFERENCES `Opticians`.`Glasses` (`idGlasses`)
    ON DELETE RESTRICT
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Supplier`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Supplier` (`idSupplier`, `name`, `addressStreet`, `addressNumber`, `addressFloor`, `addressDoor`, `addressCity`, `addressZipcode`, `addressCountry`, `phone`, `fax`, `nif`) VALUES (1, 'Superoptic', 'C. Padua', 8, '2', '1', 'Barcelona', 08023, 'Spain', 932001112, NULL, 'G12345678');
INSERT INTO `Opticians`.`Supplier` (`idSupplier`, `name`, `addressStreet`, `addressNumber`, `addressFloor`, `addressDoor`, `addressCity`, `addressZipcode`, `addressCountry`, `phone`, `fax`, `nif`) VALUES (2, 'Ulleres i més', 'C. Balmes', 357, '1', '3', 'Barcelona', 08006, 'Spain', 937891236, NULL, 'H23459865');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Brand`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (1, 'Carrera', 1);
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (2, 'Ray Ban', 1);
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (3, 'Oakley', 2);
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (4, 'Armani', 1);
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (5, 'Persol', 2);
INSERT INTO `Opticians`.`Brand` (`idBrand`, `name`, `Supplier_idSupplier`) VALUES (6, 'Police', 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`FrameModel`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (1, 2, 'Pasta', 'Negre', 80.50);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (2, 3, 'Pasta', 'Verd', 140.69);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (3, 2, 'Metàl·lica', 'Blau', 78.14);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (4, 1, 'Flotant', 'Vermell', 56.78);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (5, 1, 'Metàl·lica', 'Groc', 98.15);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (6, 4, 'Flotant', 'Marró', 74.56);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (7, 6, 'Metàl·lica', 'Negre', 67.89);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (8, 5, 'Metàl·lica', 'Negre', 63.47);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (9, 4, 'Pasta', 'Vermell', 99.87);
INSERT INTO `Opticians`.`FrameModel` (`idFrameModel`, `Brand_idBrand`, `frame`, `frameColor`, `framePrice`) VALUES (10, 6, 'Flotant', 'Blau', 136.67);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Glasses`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Glasses` (`idGlasses`, `gradLeftGlass`, `gradRightGlass`, `glassesColor`, `glassesPrice`, `FrameModel_idFrameModel`) VALUES (1, 3.75, 4.50, 'Transparent', 60, 3);
INSERT INTO `Opticians`.`Glasses` (`idGlasses`, `gradLeftGlass`, `gradRightGlass`, `glassesColor`, `glassesPrice`, `FrameModel_idFrameModel`) VALUES (2, 0.50, 0.75, 'Transparent', 35, 5);
INSERT INTO `Opticians`.`Glasses` (`idGlasses`, `gradLeftGlass`, `gradRightGlass`, `glassesColor`, `glassesPrice`, `FrameModel_idFrameModel`) VALUES (3, 2.50, 2.50, 'Solar', 30, 4);
INSERT INTO `Opticians`.`Glasses` (`idGlasses`, `gradLeftGlass`, `gradRightGlass`, `glassesColor`, `glassesPrice`, `FrameModel_idFrameModel`) VALUES (4, 1.25, 1.00, 'Solar', 45, 3);
INSERT INTO `Opticians`.`Glasses` (`idGlasses`, `gradLeftGlass`, `gradRightGlass`, `glassesColor`, `glassesPrice`, `FrameModel_idFrameModel`) VALUES (5, 0.50, .25, 'Transparent', 25, 2);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Employee`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Employee` (`idEmployee`, `name`) VALUES (1, 'Pau Vila');
INSERT INTO `Opticians`.`Employee` (`idEmployee`, `name`) VALUES (2, 'Anna Nadal');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Customer` (`idCustomer`, `name`, `address`, `phone`, `email`, `signUpDate`) VALUES (1, 'Pere', 'García', '600459874', 'peregarcia@correu.cat', '2019-11-05');
INSERT INTO `Opticians`.`Customer` (`idCustomer`, `name`, `address`, `phone`, `email`, `signUpDate`) VALUES (2, 'Joana', 'Pérez', '632894701', 'joanaperez@correu.cat', '2019-11-05');
INSERT INTO `Opticians`.`Customer` (`idCustomer`, `name`, `address`, `phone`, `email`, `signUpDate`) VALUES (3, 'Isabel', 'González', '654879120', 'isabelgonzalez@correu.cat', '2019-11-03');
INSERT INTO `Opticians`.`Customer` (`idCustomer`, `name`, `address`, `phone`, `email`, `signUpDate`) VALUES (4, 'Miquel', 'López', '654749103', 'miquellopez@correu.cat', '2019-11-02');

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Recommendation`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Recommendation` (`idRecommendation`, `recommendedCustomer`, `recommendedBy`) VALUES (1, 2, 1);
INSERT INTO `Opticians`.`Recommendation` (`idRecommendation`, `recommendedCustomer`, `recommendedBy`) VALUES (2, 3, 1);
INSERT INTO `Opticians`.`Recommendation` (`idRecommendation`, `recommendedCustomer`, `recommendedBy`) VALUES (3, 4, 3);

COMMIT;


-- -----------------------------------------------------
-- Data for table `Opticians`.`Invoice`
-- -----------------------------------------------------
START TRANSACTION;
USE `Opticians`;
INSERT INTO `Opticians`.`Invoice` (`idInvoice`, `Glasses_idGlasses`, `Customer_idCustomer`, `Employee_idEmployee`) VALUES (1, 1, 3, 2);
INSERT INTO `Opticians`.`Invoice` (`idInvoice`, `Glasses_idGlasses`, `Customer_idCustomer`, `Employee_idEmployee`) VALUES (2, 2, 4, 1);
INSERT INTO `Opticians`.`Invoice` (`idInvoice`, `Glasses_idGlasses`, `Customer_idCustomer`, `Employee_idEmployee`) VALUES (3, 3, 4, 2);
INSERT INTO `Opticians`.`Invoice` (`idInvoice`, `Glasses_idGlasses`, `Customer_idCustomer`, `Employee_idEmployee`) VALUES (4, 4, 2, 2);
INSERT INTO `Opticians`.`Invoice` (`idInvoice`, `Glasses_idGlasses`, `Customer_idCustomer`, `Employee_idEmployee`) VALUES (5, 5, 1, 1);

COMMIT;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
